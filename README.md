- [Animals Sounds](#org834efbc)
  - [License](#org9f00187)


<a id="org834efbc"></a>

# Animals Sounds

This is a sample web page to practice basic JS, HTML, and CSS. The web page listens to keyboard-keys + mouse+clicks in order to trigger an audio event (animals sounds).


<a id="org9f00187"></a>

## License

GPL v3 - <https://www.gnu.org/licenses/gpl-3.0.en.html>